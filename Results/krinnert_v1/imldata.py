#!/usr/bin/env python
import os
import sys
import numpy as np
from ROOT import gROOT, TChain
import matplotlib.pyplot as plt
from matplotlib import cm
from sklearn.decomposition import PCA
from sklearn import preprocessing

from utils import print_progress

_data_dir = '/home/kurt/workbench/iml-workshop/data'
_shapes_dir = _data_dir
_data_samples = ['quarks_standard', 'gluons_standard', 'quarks_modified', 'gluons_modified']

class JetVariables:
    def __init__(self):
        self.chain = None
        self.jet_vars = np.empty(10, dtype=np.float32)
        self.jet_var_names = [
                'mass',
                'pt',
                'phi',
                'eta',
                'ntracks',
                'ntowers',
                'trackptmax',
                'trackptlsr',
                'radial',
                'dispersion',
                ]
        self.n_jet_vars = len(self.jet_var_names)

    def connect(self, chain):
        self.chain = chain
        chain.SetBranchStatus('*',0)
        chain.SetBranchStatus('jet_vars', 1)
        chain.SetBranchAddress('jet_vars', self.jet_vars)

    def read_jets(self, test_fraction=0.0, message='loading jet data'):
        n_train = 0
        n_test = 0
        report_count = 10000
        nentries = self.chain.GetEntries()
        new_train_size = nentries
        new_test_size = nentries
        if nentries > 1000:
            new_train_size = int(nentries * (1.0 - test_fraction) * 1.1)
            new_test_size = int(nentries * test_fraction * 1.1)
        new_train = np.empty((new_train_size, self.n_jet_vars), dtype=np.float32)
        new_test = np.empty((new_test_size, self.n_jet_vars), dtype=np.float32)

        for entry in range(nentries):
            self.chain.GetEntry(entry)
            if np.random.sample() < test_fraction:
                new_test[n_test] = self.jet_vars
                n_test += 1
            else:
                new_train[n_train] = self.jet_vars
                n_train += 1
            if report_count == 10000:
                report_count = 0
                print_progress(entry, nentries, prefix=message, suffix='complete')
            report_count += 1
        print_progress(nentries, nentries, prefix=message, suffix='complete')

        return new_train[:n_train], new_test[:n_test]


def load_data(validation=False, test_fraction=0.0):
    create_missing_jet_shapes()
    if validation:
        shape_data_files = [os.path.join(_shapes_dir, sample + '_shapes.root') for sample in _data_samples[2:]]
        load_tag = 'validation'
    else:
        shape_data_files = [os.path.join(_shapes_dir, sample + '_shapes.root') for sample in _data_samples[:2]]
        load_tag = 'training'
    quark_shapes_file = shape_data_files[0]
    gluon_shapes_file = shape_data_files[1]

    jet_vars = JetVariables()

    qjets = TChain('JetShapes')
    qjets.Add(quark_shapes_file)
    jet_vars.connect(qjets)
    x_train_q, x_test_q = jet_vars.read_jets(test_fraction=test_fraction, message='loading quark jets ({})'.format(load_tag))
    n_train_q, _ = x_train_q.shape
    n_test_q, _ = x_test_q.shape
    y_train_q = np.full((n_train_q,),1)
    y_test_q = np.full((n_test_q,),1)

    gjets = TChain('JetShapes')
    gjets.Add(gluon_shapes_file)
    jet_vars.connect(gjets)
    x_train_g, x_test_g = jet_vars.read_jets(test_fraction=test_fraction, message='loading gluon jets ({})'.format(load_tag))
    n_train_g, _ = x_train_g.shape
    n_test_g, _ = x_test_g.shape
    y_train_g = np.full((n_train_g,),0)
    y_test_g = np.full((n_test_g,),0)

    print('merging arrays... ', end='', flush=True)
    x_train = np.concatenate((x_train_q, x_train_g))
    x_test = np.concatenate((x_test_q, x_test_g))
    n_train, _ = x_train.shape
    n_test, _ = x_test.shape
    y_train = np.concatenate((y_train_q, y_train_g))
    y_test = np.concatenate((y_test_g, y_test_g))
    size_train = x_train.nbytes
    size_train /= 1024*1024
    size_test = x_test.nbytes
    size_test /= 1024*1024
    print('done, training data size {:.1f} MB, test data size {:.1f} MB.'.format(size_train, size_test))

    print('preprocessing... ', end='', flush=True)
    pca = PCA(whiten=True, svd_solver='full')
    if 0 != n_train:
        x_train = pca.fit_transform(x_train)
        x_train = preprocessing.MinMaxScaler().fit_transform(x_train)
    if 0 != n_test:
        x_test = pca.fit_transform(x_test)
        x_test = preprocessing.MinMaxScaler().fit_transform(x_test)
    print('done.')

    return x_train, y_train, x_test, y_test


def create_missing_jet_shapes():
    creator_loaded = False
    for sample_dir in _data_samples:
        shape_data_file = os.path.join(_shapes_dir, sample_dir + '_shapes.root')
        if not os.path.isfile(shape_data_file):
            if not creator_loaded:
                gROOT.LoadMacro('CreateJetShapes.C+')
                from ROOT import CreateJetShapes
                creator_loaded = True
            CreateJetShapes(os.path.join(_data_dir, sample_dir), shape_data_file)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        _data_dir = sys.argv[1]
    if len(sys.argv) > 2:
        _shapes_dir = sys.argv[2]
    x_train, y_train, _, _ = load_data(test_fraction=0.0, validation=False)
    print(x_train[:8])
    print()
    print(x_train[-8:])
    plt.imshow(np.corrcoef(x_train, rowvar=0), cmap=cm.copper)
    plt.colorbar()
    plt.show()
